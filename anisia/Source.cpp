#include <iostream>

using namespace std;
namespace {
	int i = 2;
}

namespace A  {
	int i = 3;
}

int main() {
	using A::i;
	cout << i << endl;
	cout << "Helo World!" << endl;
}
